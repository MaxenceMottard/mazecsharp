﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
	private float TranslateSpeed;
	private float DefaultTranslateSpeed = 400f;
	
	// Use this for initialization
	void Start ()
	{
		TranslateSpeed = DefaultTranslateSpeed;
		transform.position = new Vector3(3000 + transform.position.x, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (transform.position.x < 100 && transform.position.x > -100)
		{
			TranslateSpeed = 20f;
		}
		else
		{
			TranslateSpeed = DefaultTranslateSpeed;
		}
		
		if (transform.position.x < - 2000)
		{
			transform.position = new Vector3(3000, transform.position.y, transform.position.z);
		}
		else
		{
			transform.Translate( new Vector3(0, 0, TranslateSpeed * Time.deltaTime) );
		}
	}
}
