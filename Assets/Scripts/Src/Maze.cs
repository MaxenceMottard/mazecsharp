using System;
using System.Collections.Generic;

namespace MazeBankCSHARP_Classes
{
    public class Maze
    {
        public Cell[,] CellTable { get; set; }
        public int Size { get; set; }
        private readonly int NumberCell;
        private int NumberVisitedCell;
        public Cell StartCell { get; set; }
        public Cell EndCell { get; set; }

        /// <summary>
        /// When the class is instantiated
        /// </summary>
        /// <param name="size">The size of the maze</param>
        public Maze(int size)
        {
            CellTable = new Cell[size, size];
            Size = size;
            NumberCell = Size * Size;
            NumberVisitedCell = 0;


            CreateTableCell();

            GenerateMaze();

            foreach (Cell cell in CellTable)
            {
                cell.IsVisited = false;
            }

            SetStartEndCells();
            SearchPath();
        }

        /// <summary>
        /// Creation of the cell table
        /// </summary>
        private void CreateTableCell()
        {
            for (int x = 0; x < Size; x++)
            {
                for (int y = 0; y < Size; y++)
                {
                    CellTable[x, y] = new Cell(x, y);
                }
            }
        }

        /// <summary>
        /// Break one wall on each cell
        /// </summary>
        private void GenerateMaze()
        {
            Stack<Cell> visitedStack = new Stack<Cell>();
            Random rnd = new Random();
            Cell target = CellTable[0, 0];

            while (NumberVisitedCell < NumberCell)
            {
                List<Cell> adjacentCells = GetAdjacentCells(target);
                if (!target.IsVisited)
                {
                    NumberVisitedCell++;
                }

                target.IsVisited = true;

                if (adjacentCells.Count == 0)
                {
                    target = visitedStack.Pop();
                }
                else
                {
                    visitedStack.Push(target);

                    int nextCellIndex = rnd.Next(0, adjacentCells.Count);
                    Cell nextCell = adjacentCells[nextCellIndex];

                    DeleteCommonWalls(target, nextCell);
                    target = nextCell;
                }
            }
        }

        /// <summary>
        ///  Random selection of start and end cells  
        /// </summary>
        private void SetStartEndCells()
        {
            Random rnd = new Random();
            int xStart = rnd.Next(0, Size - 1);
            int yStart = rnd.Next(0, Size - 1);
            StartCell = CellTable[xStart, yStart];
            StartCell.IsInPath = true;

            int xEnd;
            int yEnd;
            do
            {
                xEnd = rnd.Next(0, Size);
            } while (xStart - xEnd > -3 && xStart - xEnd < 3);

            do
            {
                yEnd = rnd.Next(0, Size);
            } while (yStart - yEnd > -3 && yStart - yEnd < 3);

            EndCell = CellTable[xEnd, yEnd];
            EndCell.IsInPath = true;
        }

        /// <summary>
        /// Draw the path between end and begin cells
        /// </summary>
        private void SearchPath()
        {
            Stack<Cell> visitedStack = new Stack<Cell>();
            Random rnd = new Random();
            Cell target = StartCell;

            while (target != EndCell)
            {
                List<Cell> adjacentCells = GetAdjacentOpenCells(target);
                target.IsVisited = true;

                if (adjacentCells.Count == 0)
                {
                    target.IsInPath = false;
                    target = visitedStack.Pop();
                }
                else
                {
                    target.IsInPath = true;
                    visitedStack.Push(target);

                    int nextCellIndex = rnd.Next(0, adjacentCells.Count);
                    target = adjacentCells[nextCellIndex];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        private List<Cell> GetAdjacentCells(Cell cell)
        {
            List<Cell> cellTable = new List<Cell>();

            int cellX = cell.Coord.X;
            int cellY = cell.Coord.Y;

            Cell TopCell;
            Cell RightCell;
            Cell BottomCell;
            Cell LeftCell;

            if (cellY > 0)
            {
                TopCell = CellTable[cellX, cellY - 1];
                if (!TopCell.IsVisited)
                {
                    cellTable.Add(TopCell);
                }
            }

            if (cellX < Size - 1)
            {
                RightCell = CellTable[cellX + 1, cellY];
                if (!RightCell.IsVisited)
                {
                    cellTable.Add(RightCell);
                }
            }

            if (cellY < Size - 1)
            {
                BottomCell = CellTable[cellX, cellY + 1];
                if (!BottomCell.IsVisited)
                {
                    cellTable.Add(BottomCell);
                }
            }

            if (cellX > 0)
            {
                LeftCell = CellTable[cellX - 1, cellY];
                if (!LeftCell.IsVisited)
                {
                    cellTable.Add(LeftCell);
                }
            }

            return cellTable;
        }

        /// <summary>
        /// Get a list of adjacent cells that are open (where walls are broken)
        /// </summary>
        /// <param name="cell">The cell for which we are looking for open adjacent cells</param>
        /// <returns>List of adjacent open cells</returns>
        private List<Cell> GetAdjacentOpenCells(Cell cell)
        {
            List<Cell> cellTable = new List<Cell>();

            int cellX = cell.Coord.X;
            int cellY = cell.Coord.Y;

            Cell TopCell;
            Cell RightCell;
            Cell BottomCell;
            Cell LeftCell;

            if (!cell.Top)
            {
                TopCell = CellTable[cellX, cellY - 1];
                if (!TopCell.IsVisited)
                {
                    cellTable.Add(TopCell);
                }
            }

            if (!cell.Right)
            {
                RightCell = CellTable[cellX + 1, cellY];
                if (!RightCell.IsVisited)
                {
                    cellTable.Add(RightCell);
                }
            }

            if (!cell.Bottom)
            {
                BottomCell = CellTable[cellX, cellY + 1];
                if (!BottomCell.IsVisited)
                {
                    cellTable.Add(BottomCell);
                }
            }

            if (!cell.Left)
            {
                LeftCell = CellTable[cellX - 1, cellY];
                if (!LeftCell.IsVisited)
                {
                    cellTable.Add(LeftCell);
                }
            }

            return cellTable;
        }

        /// <summary>
        /// Breaking down the common walls between two cells
        /// </summary>
        /// <param name="firstCell">The first cell for which we want to break the wall</param>
        /// <param name="secondCell">The second cell for which we want to break the wall</param>
        private void DeleteCommonWalls(Cell firstCell, Cell secondCell)
        {
            if (firstCell.Coord.X == secondCell.Coord.X - 1)
            {
                //    CELL AT RIGHT
                firstCell.Right = false;
                secondCell.Left = false;
            }
            else if (firstCell.Coord.X == secondCell.Coord.X + 1)
            {
                //    CELL AT LEFT
                firstCell.Left = false;
                secondCell.Right = false;
            }
            else if (firstCell.Coord.Y == secondCell.Coord.Y - 1)
            {
                //    CELL AT BOTTOM
                firstCell.Bottom = false;
                secondCell.Top = false;
            }
            else
            {
                //    CELL AT TOP
                firstCell.Top = false;
                secondCell.Bottom = false;
            }
        }
    }
}